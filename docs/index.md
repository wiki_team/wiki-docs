# 運用ガイドライン

本サイトでは各案件の運用ガイドラインを管理します。

## 実装機能
ガイドライン内容の表示

## 未実装機能
ページの追加や削除、文言の追記、編集、削除　※実現できるか未確認

## 学習用リンク
[MKDocs – 拡張プラグイン集](https://dev.classmethod.jp/articles/mkdocs-plugins-1/)

## レイアウト調整
### 注釈関連
---
!!! Note
    Note内容を記載
 
!!! Tip
    Tip内容を記載
 
!!! Success
    成功内容を記載
 
!!! Failure
    失敗内容を記載
 
!!! Warning
    警告内容を記載
 
!!! Danger
    エラー内容を記載
 
!!! Bug
    バグ内容を記載
 
!!! summary
    要約内容を記載

### ソースコード表記
---
    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
