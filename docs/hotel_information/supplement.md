# 補足情報
---
## はじめに
---

### 各環境について
---

### 必要なツールのインストールについて
---
1. GoggleChromeに以下、拡張機能 [WindowResizer](https://chrome.google.com/webstore/detail/window-resizer/kkelicaakdanhinjdeammmilcgefonfh?hl=ja)をインストールする
1. 「Window Resizer」を有効化する
1. 固定サイズ（viewportを960×540）を設定する　※縦960、横540で「viewport」を固定する。「window」ではないので注意  

## 画像に関する作業手順
### スクリーンショット画像の取得
---
1. GoogleChromeで対象のページを開く
1. 「Window Resizer」のメニューアイコンをクリックする
1. 設定済みの固定サイズ（viewportを960×540）をクリックする
1. Webブラウザのウィンドウサイズが設定サイズにリサイズされるので、その状態で開発者ツールを表示（「command + option + I」）する
1. ウィンドウ表示を最適化する為、Webブラウザのリロードボタンをクリックする　※リサイズによって表示内容が崩れる為、再度読み込み直す必要がある
1. リロード後、画面の縦横スクロールが発生していないことを確認　※表示サイズがウィンドウサイズと合っていることを確認
1. 開発者モード（「command + shift + P」をクリック）を開き、入力欄に「full」と入力後、「Enter」を押すことでスクショ画像をダウンロードできます。  

### 画像のリサイズ
---
1. 対象の画像を全て一括で選択し、開く
1. メニュー「ツール」→「サイズを調整」をクリックする
1. 表示されたダイアログ内の設定を以下にする
   1. 「サイズを合わせる」を「カスタム」
   1. 「幅」、「高さ」を固定サイズ（960×540）、単位はピクセル　※入力前に「縦横比を固定」のチェックを外すと良い。「イメージを再サンプル」はチェックのままで良い
   1. 解像度をデフォルト値（おそらく：72）、単位はピクセル/インチ

1. 「OK」をクリックすることでリサイズ完了

## 画像について
---
* スクショ画像を固定サイズ（960×540）にリサイズする。そのままだと画像が大きいのと容量が重い
* 画像とインライン指定の縦横サイズは完全一致すること<br>
  ※ズレるとテレビで表示されないことがある<br>
  ※インライン指定は、主にデザインするための意味合いと、画像をどのサイズで作成するかの確認用<br>
  その指定したサイズで画像を作る必要があり、元画像は大きいのリサイズしながら切り抜く<br><br>

* デザインしたサイズに画像をリサイズする<br>
 ※デザインした枠に画像をリサイズしてはめ込むので、元画像と縦横比は変わって良い<br>
 ※切り抜いてみたら画像の端が予想以上に切れてしまう場合は他の素材を探し直す<br><br>

* 最終的にはデザインのサイズに合わせる　→　例）160ｘ120
* アスペクト比を維持したまま理想なサイズに近づけようとしてもピッタリ縦横サイズを合わないことの方が多い。そのため、例えば、横幅を基準に160でリサイズした際、高さが120に満たない場合は高さを基準でリサイズする。で、目標の画像サイズ（埋め込もうとしている枠サイズ）で切り抜くことで枠に画像をはめ込む<br><br>

* 大きな素材はリサイズして小さくする
* デザインの縦横比と素材の縦横比が違う事が多いので、リサイズして小さくするが縦横比が違う場合は余白が出ない程度に小さくして余白分は捨てる（切り抜く）

#### サイトからダウンロードした元画像（640×480）
---
![./img/site_1_.jpg](./img/site_1_.jpg)

#### 実際に表示するサイズにリサイズ→切り抜きした画像（160×120）
---
![./img/site_1.jpg](./img/site_1.jpg)